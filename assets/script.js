function openSlideMenu() {
  document.getElementById('menu').style.width = '300px';
  document.getElementById('navContent').style.marginLeft = '300px';
  filtre();
};

function closeSlideMenu() {
  if (screen.width < 426) {
    document.getElementById('menu').style.width = '0';
    document.getElementById('navContent').style.marginLeft = '0';
    noFiltre();
  }
};

function filtre() {
  document.getElementById("filtre").className += "filtre";
}

function noFiltre() {
  document.getElementById("filtre").className = document.getElementById("filtre").className.replace(/(?:^|\s)filtre(?!\S)/g, "")
}



var offsetTop = $(".idiomas").offset().top;
$(window).scroll(function () {
  var height = $(window).height();
  if ($(window).scrollTop() + height > offsetTop) {
    jQuery(".progress-bar").each(function () {
      jQuery(this).find(".progress-content").animate({
          width: jQuery(this).attr("data-percentage")
        },
        2000
      );

      jQuery(this).find(".progress-number-mark").animate({
        left: jQuery(this).attr("data-percentage")
      }, {
        duration: 2000,
        step: function (now, fx) {
          var data = Math.round(now);
          //jQuery(this).find(".percent").html(data + "%");
        }
      });
    });
  }
});

jQuery(function () {
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 20) {
        $('#fleche').css('opacity', '1');
        $('nav').css('background-color', 'rgba(15, 15, 15, 1)');
      } else {
        $('#fleche').removeAttr('style');
        $('nav').removeAttr('style');
      }
    });
  });
});


class TypeWriter {
  constructor(txtElement, words, wait = 3000) {
    this.txtElement = txtElement;
    this.words = words;
    this.txt = '';
    this.wordIndex = 0;
    this.wait = parseInt(wait, 10);
    this.type();
    this.isDeleting = false;
  }

  type() {
    // Current index of word
    const current = this.wordIndex % this.words.length;
    // Get full text of current word
    const fullTxt = this.words[current];

    // Check if deleting
    if (this.isDeleting) {
      // Remove char
      this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
      // Add char
      this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    // Insert txt into element
    this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

    // Initial Type Speed
    let typeSpeed = 100;

    if (this.isDeleting) {
      typeSpeed /= 2;
    }

    // If word is complete
    if (!this.isDeleting && this.txt === fullTxt) {
      // Make pause at end
      typeSpeed = this.wait;
      // Set delete to true
      this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
      this.isDeleting = false;
      // Move to next word
      this.wordIndex++;
      // Pause before start typing
      typeSpeed = 200;
    }

    setTimeout(() => this.type(), typeSpeed);
  }
}


// Init On DOM Load
document.addEventListener('DOMContentLoaded', init);

// Init App
function init() {
  const txtElement = document.querySelector('.txt-type');
  const words = JSON.parse(txtElement.getAttribute('data-words'));
  const wait = txtElement.getAttribute('data-wait');
  // Init TypeWriter
  new TypeWriter(txtElement, words, wait);
}