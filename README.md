# CV site personnel

Ceci est mon propre CV site :-)

## Lien du site

[manuelhernandez.fr](https://manuelhernandez.fr/)

## Fabriqué avec

* [VS Code](https://code.visualstudio.com/) - Editeur de code
* [jQuery](https://jquery.com/) - Librairie JavaScript
* [AOS](https://michalsnik.github.io/aos/) - Librairie d'animations sur le scroll
* [LightWidget](https://lightwidget.com/) - Widget Instagram